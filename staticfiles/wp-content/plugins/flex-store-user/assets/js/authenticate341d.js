/**
 * Created by Manhn on 9/5/2017.
 */
jQuery(document).ready(function ($) {
    $('.fs-form').on('click', '.fs-signup-show,.fs-login-show', function (event) {
        event.preventDefault();
        var wrapper = $(this).parents('.fs-user-wrapper');
        var fsform  = $(this).parents('.fs-form');
        wrapper.find('.fs-form').addClass('active');
        fsform.removeClass('active');
        fsform.find('.fs-signup-notice').html('');
        fsform.find('.fs-login-notice').html('');
    });
    var login_form = $('form.fs-login-form');
    login_form.each(function () {
        $(this).validate();
    });
    login_form.on('submit', function (event) {
        var id = $(this).attr('id');
        event.preventDefault();
        
        var $refer1 = $('#' + id + ' input[name="refer"]').val();
        var $refer2 = $('#' + id + ' input[name="_wp_http_referer"]').val();
        
        var $refer   = ($refer1.length > 0) ? $refer1 : $refer2;
        var username = $('#' + id + ' input[name="username"]').val();
        var password = $('#' + id + ' input[name="password"]').val();
        if (!$(this).valid()) {
            return false;
        }
        
        var remember  = $('#' + id + ' input[name="remember"]').val();
        var remember_ = false;
        if (remember.length > 0) {
            remember_ = true;
        }
        $.ajax({
            url: data_auth.ajax,
            data: {
                action: data_auth.action.login,
                data: {
                    username: $('#' + id + ' input[name="username"]').val(),
                    password: $('#' + id + ' input[name="password"]').val(),
                    remember: remember_
                }
            },
            dataType: 'json',
            method: 'POST',
        }).done(function (response) {
            $('#' + id + ' .fs-login-notice').html(response.message);
            
            if (response.type == 'success') {
                window.location = $refer;
            }
        }).fail(function (response) {
        
        }).always(function (response) {
        
        });
    });
    var signup_form = $('form.fs-signup-form');
    signup_form.each(function () {
        $(this).validate({
            rules: {
                fs_password_re: {
                    equalTo: '[name="fs_password"]'
                },
                fs_email_re: {
                    equalTo: '[name="fs_email"]'
                }
            }
        });
    });
    signup_form.on('submit', function (event) {
        var id = $(this).attr('id');
        event.preventDefault();
        $('#' + id + ' .fs-signup-notice').html('');
        var $refer1 = $('#' + id + ' input[name="refer"]').val();
        var $refer2 = $('#' + id + ' input[name="_wp_http_referer"]').val();
        
        var error    = [];
        var $refer   = ($refer1.length > 0) ? $refer1 : $refer2;
        var input    = $('#' + id + ' input[name^="fs_"]');
        var elements = this.elements;
        var data     = {};
        for (var i = 0; i < elements.length; i++) {
            var element        = elements[i];
            data[element.name] = element.value;
        }
        if(!$(this).valid()){
            return false;
        }
        $.ajax({
            url: data_auth.ajax,
            data: {
                action: data_auth.action.signup,
                data: Object.assign({}, data)
            },
            dataType: 'json',
            method: 'POST'
        }).done(function (response) {
            if (response.type === 'error') {
                $('#' + id + ' .fs-signup-notice').html('<p>' + response.message + '</p>');
            }
            if (response.type === 'success') {
                $('#' + id + ' .fs-signup-notice').html('<p>' + response.message + '</p>');
                setTimeout(function(){
                    window.location = $refer;
                },5000);
            }
            if (response.type === 'wait') {
                $('#' + id).slideUp();
                $('#' + id).parent().find('.fs-signup-notice').html('<h4>' + response.message + '</h4>');
            }
        }).fail(function (response) {
        
        }).always(function (response) {
        
        });
    });
});

jQuery(document).ready(function ($) {
    $(document).on('click', '#signup_show,#login_show',function (event) {
        event.preventDefault();
        $('.form').addClass('active');
        $(this).parents('.form').removeClass('active');
        $('#fs_signup_notice').html('');
        $('#fs_login_notice').html('');
    });
})