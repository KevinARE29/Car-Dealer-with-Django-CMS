/**
 * Created by Manhn on 20/6/2017.
 */
jQuery(document).ready(function ($) {
    var form = $('form.fs-contact-seller-form');
    form.validate();
    form.on('submit', function (e) {
        e.preventDefault();
        var name_    = $(this).find('[name="fs_customer_name"]');
        var name     = name_.val();
        var email_   = $(this).find('[name="fs_customer_email"]');
        var email    = email_.val();
        var message_ = $(this).find('[name="fs_customer_message"]');
        var message  = message_.val();
        var nonce_   = $(this).find('[name="fs_contact_seller"]');
        var nonce    = nonce_.val();
        var seller_  = $(this).find('[name="fs_seller_id"]');
        var seller   = seller_.val();
        
        if (form.valid()) {
            $.ajax({
                url: fs_data.url,
                method: 'post',
                data: {
                    action: fs_data.action,
                    name: name,
                    email: email,
                    message: message,
                    nonce: nonce,
                    seller: seller
                },
                async: false,
                dataType: 'json',
                beforeSend: function () {
                }
            }).done(function (response) {
                if(response.type=='success'){
                    var _notice = $(".fs-notice");
                    _notice.html(response.message);
                    name_.val('');
                    email_.val('');
                    message_.val('');
                    setTimeout(function () {
                        _notice.html('');
                    },3000);
                }
                form.find('.message').html(response.message);
            }).fail(function (response) {
            
            }).always(function (response) {
            })
        }
    });
});