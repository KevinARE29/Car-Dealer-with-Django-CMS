(function($) {
    "use strict";

    $(document).ready(function () {

        $('.fs-vehicle-bxslider').find('.cms-vehicle-images').each(function() {
            $('#cms-vehicle-thumbnail', $(this)).flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 139,
                itemMargin: 15,
                asNavFor: $('.cms-vehicle-slider', $(this))
            });
             
            $('.cms-vehicle-slider', $(this)).flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: $('#cms-vehicle-thumbnail', $(this)),
            });
        });

        $('.fs-vehicle-bxslider2').find('.cms-vehicle-images').each(function() {
            $('#cms-vehicle-thumbnail', $(this)).flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 84,
                itemMargin: 15,
                asNavFor: $('.cms-vehicle-slider', $(this))
            });
             
            $('.cms-vehicle-slider', $(this)).flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: $('#cms-vehicle-thumbnail', $(this)),
            });
        });

    });

}(jQuery));