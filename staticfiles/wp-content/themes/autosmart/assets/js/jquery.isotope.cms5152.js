(function($){
    $(document).ready(function(){
      setTimeout(function(){ 

        var iso = new Isotope( '.fs-latest-car-grid-content', {
          itemSelector:'.grid-item',
          layoutMode: 'fitRows',
          getSortData: {
              number: '.hp-number parseInt',
          },
          sortBy : 'number',
          sortAscending : false
        })

        var filterFns = {
          ium: function( itemElem ) {
            var name = itemElem.querySelector('.name').textContent;
            return name.match( /ium$/ );
          }
        };

        if ($('.fs-latest-filter-wrap').hasClass('fs-latest-car-filter')) {
          var filtersElem = document.querySelector('.fs-latest-car-filter');
          filtersElem.addEventListener( 'click', function( event ) {
            if ( !matchesSelector( event.target, 'button' ) ) {
              return;
            }
            var filterValue = event.target.getAttribute('data-filter');
            filterValue = filterFns[ filterValue ] || filterValue;
            iso.arrange({ filter: filterValue });
          });
          var buttonGroups = document.querySelectorAll('.fs-latest-car-filter');
          for ( var i=0, len = buttonGroups.length; i < len; i++ ) {
            var buttonGroup = buttonGroups[i];
            radioButtonGroup( buttonGroup );
          }
          var sortByGroup = document.querySelector('.fs-latest-car-filter');
          sortByGroup.addEventListener( 'click', function( event ) {
            if ( !matchesSelector( event.target, '.fs-filter-item' ) ) {
              return;
            }
            var sortValue = event.target.getAttribute('data-sort-value');
            iso.arrange({ sortBy: sortValue });
          });
        }

        function radioButtonGroup( buttonGroup ) {
          buttonGroup.addEventListener( 'click', function( event ) {
            if ( !matchesSelector( event.target, 'button' ) ) {
              return;
            }
            buttonGroup.querySelector('.fs-filter-active').classList.remove('fs-filter-active');
            event.target.classList.add('fs-filter-active');
          });
        }

      }, 300);

    });
})(jQuery);