jQuery(document).ready(function($) {
    "use strict";
    /* window */
    var window_width, window_height, scroll_top;
    /* admin bar */
    var adminbar = $('#wpadminbar');
    var adminbar_height = 0;
    /* header menu */
    var header = $('#cshero-header');
    var header_top = 0;
    /* scroll status */
    var scroll_status = '';
    /**
     * window load event.
     * 
     * Bind an event handler to the "load" JavaScript event.
     * @author Fox
     */
    $(window).on('load', function() {
        /** current scroll */
        scroll_top = $(window).scrollTop();
        /** current window width */
        window_width = $(window).width();
        /** current window height */
        window_height = $(window).height();
        /* get admin bar height */
        adminbar_height = adminbar.length > 0 ? adminbar.outerHeight(true) : 0;
        /* get top header menu */
        header_top = header.length > 0 ? header.offset().top - adminbar_height : 0;
        /* check sticky menu. */
        cms_stiky_menu();
        cms_enscroll();
        cms_col_sameheight();
        cms_section_offset();
        cms_auto_video_width();
        cms_fs_search();
        cms_page_loading();
        cms_theia_sticky_sidebar();

        $(".cms-page-in").css('opacity','1');
        
    });
    /**
     * reload event.
     * 
     * Bind an event handler to the "navigate".
     */
    $(window).unload(function() {
        if (typeof cms_page_loading !== 'undefined') cms_page_loading(1);
    });
    /**
     * resize event.
     * 
     * Bind an event handler to the "resize" JavaScript event, or trigger that event on an element.
     * @author Fox
     */
    $(window).on('resize', function(event, ui) {
        /** current window width */
        window_width = $(event.target).width();
        /** current window height */
        window_height = $(window).height();
        /** current scroll */
        scroll_top = $(window).scrollTop();
        /* check sticky menu. */
        cms_stiky_menu();
        cms_enscroll();
        cms_col_sameheight();
        cms_section_offset();
        cms_auto_video_width();
        cms_fs_search();
    });
    /**
     * scroll event.
     * 
     * Bind an event handler to the "scroll" JavaScript event, or trigger that event on an element.
     * @author Fox
     */
    var lastScrollTop = 0;
    $(window).on('scroll', function() {
        /** current scroll */
        scroll_top = $(window).scrollTop();
        if(scroll_top < lastScrollTop) {
            /* scroll up. */
            scroll_status = 'up';
        } else {
            /* scroll down. */
            scroll_status = 'down';
        }
        // scroll_status == 'up'
        lastScrollTop = scroll_top;
        /* check sticky menu. */
        cms_stiky_menu();
    });
    /**
     * Stiky menu
     *
     * Show or hide sticky menu.
     * @author Fox
     * @since 1.0.0
     */
    function cms_stiky_menu() {
        var h_header_wrap = $('#cshero-header-inner').height();

        if ($('#cshero-header-inner').hasClass('sticky-desktop') && scroll_top > h_header_wrap && window_width > 991 && scroll_status == 'up') {
            header.addClass('header-fixed');
            header.addClass('header-fixed-hidden');
            $('body').addClass('hd-fixed-hidden');
            $('body').addClass('hd-fixed');
            $('#back_to_top').addClass('on');
        } 
        if ($('#cshero-header-inner').hasClass('sticky-desktop') && scroll_top < 550 && window_width > 991 && scroll_status == 'up') {
            header.removeClass('header-fixed');
            $('body').removeClass('hd-fixed');
            $('#back_to_top').removeClass('on');
        }

        if ($('#cshero-header-inner').hasClass('sticky-desktop') && scroll_top < 400 && window_width > 991 && scroll_status == 'up') {
            header.removeClass('header-fixed-hidden');
            $('body').removeClass('hd-fixed-hidden');
        }
    }
    /**
     * Page Loading.
     */
    function cms_page_loading($load) {
        switch ($load) {
        case 1:
            $('#cms-loadding').css('display','block')
            break;
        default:
            $('#cms-loadding').css('display','none')
            break;
        }
    }
    /**
     * Mobile menu
     * 
     * Show or hide mobile menu.
     * @author Fox
     * @since 1.0.0
     */
    $("#cshero-menu-mobile .open-menu").on('click', function() {
        $('#cshero-header-navigation').toggleClass('navigation-open');
    })
     $(".btn-buy-close").on('click',function(){
        $('#cms-buy-button-fixed').toggleClass('hide');
    })
    $(".h-cart-wrapper .h-cart").on('click', function() {
        $('.cshero-header-topbar-button .widget_shopping_cart').toggleClass('open');
    })
    /**
     * Back To Top
     * 
     * @author Fox
     * @since 1.0.0
     */
    $('body').on('click', '#back_to_top', function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1500);
    });

    $("li.has_full_width").parents('#page-wrapper').append( "<div class='bg-menu-overlay'></div>" );
    $("li.has_full_width").hover(
      function () {
        $(this).addClass('item-hover');
        $(this).parents('#page-wrapper').addClass("mega-overlay");
      },
      function () {
        $(this).removeClass('item-hover');
        $(this).parents('#page-wrapper').removeClass("mega-overlay");
      }
    );

    /**
     * Auto width video iframe
     * 
     * Youtube Vimeo.
     * @author Fox
     */
    function cms_auto_video_width() {
        $('.entry-video iframe').each(function() {
            var v_width = $(this).width();
            v_width = v_width / (16 / 9);
            $(this).attr('height', v_width + 35);
        })
    }
    /* Video Light Box */
    $('.cms-button-video').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
    $('#searchform').find("input[type='text']").each(function(ev) {
        if (!$(this).val()) {
            $(this).attr("placeholder", "Search Keywords");
        }
    });
    $('.news-tab-wrapper #searchform').find("input[type='text']").each(function(ev) {
        if (!$(this).val()) {
            $(this).attr("placeholder", "Search news");
        }
    });
    $('.tnp-field-email').find("input[type='email']").each(function(ev) {
        if (!$(this).val()) {
            $(this).attr("placeholder", "Email Address");
        }
    });
    $('.tnp-field-firstname').find("input[type='text']").each(function(ev) {
        if (!$(this).val()) {
            $(this).attr("placeholder", "Name");
        }
    });
    /* Scrolling Sidebar */
    function cms_scrolling_sidebar() {
        var offset = $("#sidebar").offset();
        var topPadding = 60;
        $(window).scroll(function() {
            if ($(window).scrollTop() > offset.top) {
                $("#sidebar").stop().animate({
                    marginTop: $(window).scrollTop() - offset.top + topPadding
                });
            } else {
                $("#sidebar").stop().animate({
                    marginTop: 0
                });
            };
        });
    }
    /**
     * Enscroll
     * 
     * 
     * @author Fox
     */
    function cms_enscroll() {
        $('.h-cart-wrapper .widget_shopping_cart_content ').enscroll();
        $('.fs-vehicle-list .fs-vehicle-images').each(function() {
            var h_auto_g = $(this).find('.fs-vehicle-gallery').height();
            if (h_auto_g > 165 && $(window).width() > 768) {
                $(this).find('.fs-vehicle-gallery').css('height', 165 + 'px');
                $(this).find('.fs-vehicle-gallery').enscroll();
            }
            if (h_auto_g > 278 && $(window).width() < 768) {
                $(this).find('.fs-vehicle-gallery').css('height', 278 + 'px');
                $(this).find('.fs-vehicle-gallery').enscroll();
            }
        })
    }

    function cms_col_sameheight() {
        $('.columns-same .col-same').matchHeight();
        if ($(window).width() > 768) {
            $('.fs-compare-head .fs-compare-col').matchHeight();
            $('.fs-brand-search-contents .fs-brand-search-item-inner').matchHeight();
            $('.fs-car-columns .fs-col-same').matchHeight();
            $('.products > .type-product').matchHeight();
        }
        if ($(window).width() > 991) {
            $('.multicolumn > li').matchHeight();
        }
    }
    /* Woo Add Class */
    $('.plus').on('click', function() {
        $(this).parent().find('input[type="number"]').get(0).stepUp();
    });
    $('.minus').on('click', function() {
        $(this).parent().find('input[type="number"]').get(0).stepDown();
    });
    /* CMS Image Popup */
    $('.cms-images-zoom').each(function() {
        $(this).magnificPopup({
            delegate: 'a.z-view',
            type: 'image',
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-fade',
        });
    });
    $('.cms-image-zoom').magnificPopup({
        delegate: 'a.z-view', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: false
        },
        mainClass: 'mfp-fade',
        // other options
    });
    $('.fs-vehicle-content').each(function() {
        $(this).find('.cms-images-zoom').magnificPopup({
            delegate: 'a.z-view',
            type: 'image',
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-fade',
        });
    });
    /* Add Class Input Contact Form */
    $(".wpcf7-form-control").focus(function() {
        $('.wpcf7-row-two').removeClass('input-filled-active');
        $(this).parents('.wpcf7-row-two').addClass('input-filled-active');
    })
    $(".wpcf7-form-control").focusout(function() {
        $(this).parents('.wpcf7-row-two').removeClass('input-filled-active');
    });
    var filled = $(".wpcf7-form-control").val();
    if (filled == '') {
        $('.wpcf7-form-control').parents('.wpcf7-row-two').removeClass('input-filled-hold');
    }
    $(".wpcf7-form-control").change(function() {
        $(this).parents('.wpcf7-row-two').addClass('input-filled-hold');
    });
    /* Section Offset */
    function cms_section_offset() {
        if ($(window).width() > 1370) {
            var w_desktop = $(window).width();
            var w_padding = (w_desktop - 1340) / 2;
            $('.section-offset-left > .vc_column-inner').css('padding-left', w_padding + 'px');
            $('.section-offset-right > .vc_column-inner').css('padding-right', w_padding + 'px');
            $('.vc_row-no-padding .fs-categories-wrap .fs-categories-carousel .owl-controls').css('right', w_padding + 'px');
        }
    }

    function cms_fs_search() {
        if ($(window).width() > 991) {
            var h_box_search = $('.fs-search-form.box-fixed').height();
            var h_box_search_heading = $('.fs-search-form.box-fixed .fs-search-heading').height() + 40;
            var h_box_search_main = h_box_search - h_box_search_heading;

            $('.fs-search-form.box-fixed').css('bottom', -h_box_search_main + 'px');
        }
        $('.fs-search-button.show-seach').each(function() {
            $(this).click(function() {
                $(this).parents('.fs-search-form.box-fixed').addClass('open');
            })
        })
        $('.show-search-vehicle').each(function() {
            $(this).click(function() {
                $('.fs-search-form.box-fixed').addClass('open');
            })
        })
        $('.fs-search-button.hidden-seach').each(function() {
            $(this).click(function() {
                $(this).parents('.fs-search-form.box-fixed').removeClass('open');
            })
        })

    }

    $('.fss-filter-label').each(function() {
        $(this).click(function() {
            $(this).toggleClass('open');
            $(this).parent().find('.fss-filter-contents').slideToggle(300);
        })
    })
    $('#widget-area > aside .wg-title').parent().addClass('wg-title-show');
    $('.wpb_widgetised_column .wpb_wrapper > aside .wg-title').parent().addClass('wg-title-show');
    $('#sidebar-bottom > aside .wg-title').parent().addClass('wg-title-show');
    $( ".single-image-zoom-trigger" ).click(function() {
      $( ".cms-images-zoom .flex-active-slide a" ).trigger( "click" );
    });

    $('.grid-image-zoom-trigger').each(function() {
        $(this).click(function() {
          $(this).parents('.fs-vehicle-grid').find('.fs-vehicle-gallery .z-view-first').trigger( 'click' );
        });
    })
    $('.fs-search-extra').each(function() {
        $(this).click(function() {
            $(this).toggleClass('open');
            $(this).parent().find('.fs-search-row-hide').toggle('display');
        });
    })
    $('[data-toggle="tooltip"]').tooltip();
    $('.flex-active').parents('.woocommerce-product-gallery').addClass('flex-active');

    function cms_theia_sticky_sidebar() {
        var offsetTop = 60;
        if ($('#cshero-header-inner').hasClass('sticky-desktop')) {
            offsetTop = 114;
        }
        $("#sidebar").theiaStickySidebar({
            "containerSelector": "",
            "additionalMarginTop": offsetTop,
            "additionalMarginBottom": "0",
            "updateSidebarHeight": false,
            "minWidth": "768",
            "sidebarBehavior": "modern"
        });
    }
});