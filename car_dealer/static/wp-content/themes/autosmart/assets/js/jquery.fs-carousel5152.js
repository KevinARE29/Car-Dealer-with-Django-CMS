(function($) {
    "use strict";

    $(document).ready(function () {
        setTimeout(function(){
            $('.fs-carousel').each(function() {

                $(this).owlCarousel({
                    loop    :($(this).attr('data-loop')) == '1' ? true : false,
                    autoplay    :($(this).attr('data-autoplay')) == '1' ? true : false,
                    dots    :($(this).attr('data-dots')) == '1' ? true : false,
                    nav    :($(this).attr('data-nav')) == '1' ? true : false,
                    margin  :parseInt($(this).attr('data-margin')),
                    responsive:{
                        0:{
                            items:parseInt($(this).attr('data-item-xs'))
                        },
                        768:{
                            items:parseInt($(this).attr('data-item-sm'))
                        },
                        992:{
                            items:parseInt($(this).attr('data-item-md'))
                        },
                        1200:{
                            items:parseInt($(this).attr('data-item-lg'))
                        }
                    }
                })

            });
        }, 300);
        
        /* OWL Filter */
        $('.fs-carousel-latest').each(function() {
            
            var owl = $(this).owlCarousel({
                loop    :($(this).attr('data-loop')) == '1' ? true : false,
                autoplay    :($(this).attr('data-autoplay')) == '1' ? true : false,
                dots    :($(this).attr('data-dots')) == '1' ? true : false,
                margin  :parseInt($(this).attr('data-margin')),
                responsive:{
                    0:{
                        items:parseInt($(this).attr('data-item-xs'))
                    },
                    768:{
                        items:parseInt($(this).attr('data-item-sm'))
                    },
                    992:{
                        items:parseInt($(this).attr('data-item-md'))
                    },
                    1200:{
                        items:parseInt($(this).attr('data-item-lg'))
                    }
                }
            })
            var owlAnimateFilter = function(even) {
                $(this)
                .addClass('item-loading')
                .delay(70 * $(this).parent().index())
                .queue(function() {
                    $(this).dequeue().removeClass('item-loading')
                })
            }

            $('.fs-latest-car-filter').on('click', '.fs-filter-item', function(e) {
                var filter_data = $(this).data('filter');
                if($(this).hasClass('fs-filter-active')) return;
                $(this).addClass('fs-filter-active').siblings().removeClass('fs-filter-active');
                owl.owlFilter(filter_data, function(_owl) { 
                    $(_owl).find('.item').each(owlAnimateFilter); 
                });
            })
        });

    });
}(jQuery));



