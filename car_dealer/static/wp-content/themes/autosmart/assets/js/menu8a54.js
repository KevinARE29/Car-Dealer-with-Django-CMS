(function ($) {
    "use strict";
    $(document).ready(function(){
        var $menu = $('.main-navigation');
        $menu.find('li').each(function(){
            var $submenu = $(this).find('> ul');
            if($submenu.length == 1){
                $(this).on('hover', function(){
                    if($submenu.offset().left + $submenu.width() > $(window).width()){
                        $submenu.addClass('back');
                    }else if($submenu.offset().left < 0){
                        $submenu.addClass('back');
                    }
                });
            }
        });
        
        /* Menu drop down*/
        $('.nav-menu li.menu-item-has-children').append('<span class="cs-menu-toggle"></span>');
        $('.menu-item-has-children > a').on('click', function(){
            $(this).parent().find('> .sub-menu').toggleClass('submenu-open');
            $(this).parent().find('> .sub-menu').slideToggle();
        });
        $('.cs-menu-toggle').on('click', function(){
            $(this).parent().find('> .sub-menu').toggleClass('submenu-open');
            $(this).parent().find('> .sub-menu').slideToggle();
        });
    });
    
})(jQuery);
