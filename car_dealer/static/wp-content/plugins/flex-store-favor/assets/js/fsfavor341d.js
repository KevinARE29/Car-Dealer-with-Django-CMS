/**
 * Created by Manhn on 3/6/2017.
 */
jQuery(document).ready(function ($) {
    if ($('.fs-favor').length) {
        $('.fs-favor').on('click', function (e) {
            e.preventDefault();
            if(!fsfavor.login){
                window.location.href = fsfavor.login_url;
            }
            var id = $(this).data('id');
            var that = this;
            $.ajax({
                url: fsfavor.ajax,
                type: 'POST',
                data: {
                    action: fsfavor.action,
                    favor_id: id
                },
                dataType: 'json',
                beforeSend: function () {
                    $(that).addClass('pending');
                }
            }).done(function (response) {
                if (response == "success") {
                    $(that).toggleClass('active');
                }
            }).fail(function (response) {
            }).always(function () {
                $(that).removeClass('pending');
            });
        });
    }
});