/**
 * @team: FsFlex Team
 * @since: 1.0.0
 * @author: KP
 */
jQuery(window).load(function () {
    function fs_user_map() {
        if (jQuery('#fs-user-map').length > 0) {
            var map = new google.maps.Map(document.getElementById('fs-user-map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 18
            });
            var geocoder = new google.maps.Geocoder();
            geocodeAddress(geocoder, map);
            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('fs-address').value;
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === 'OK') {
                        resultsMap.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        }
    }

    if (typeof google === 'object' && typeof google.maps === 'object') {
        fs_user_map();
    } else {
        var script = document.createElement("script");
        script.type = "text/javascript";
        var async = document.createAttribute('async');
        var defer = document.createAttribute('defer');
        script.setAttributeNode(async);
        script.setAttributeNode(defer);
        script.src = 'http://maps.google.com/maps/api/js?key=AIzaSyCcg3agStN0rvuTgEKEibPtnQxcUyQVCeM&callback=fs_user_map';
        document.body.appendChild(script);
    }
});