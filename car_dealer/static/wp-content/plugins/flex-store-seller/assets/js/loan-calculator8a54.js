/**
 * Created by Manhn on 22/6/2017.
 */
jQuery(document).ready(function($) {
    $.fn.digits = function(){ 
        return this.each(function(){ 
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
        })
    }
    function caculate(price, rate, periods, deposit) {
        price = parseFloat(price);
        rate = parseFloat(rate);
        periods = parseFloat(periods);
        deposit = parseFloat(deposit);
        var finance = price - deposit;
        var totalpay = finance;
        var monthlypay = 0;
        var interest = 0;
        if (rate > 0 && periods > 0) {
            var $rate = (rate / 12) / 100;
            var monthlyrate = ($rate * Math.pow((1 + $rate), periods)) / (Math.pow((1 + $rate), periods) - 1);
            monthlypay = finance * monthlyrate;
            totalpay = monthlypay * periods;
            interest = (totalpay - finance);
        }
        return {
            monthlypay: monthlypay,
            totalpay: totalpay,
            interest: interest
        }
    }
    var price_slider = $('.fs-caculator-wrapper .slider .price .range');
    var interest_slider = $('.fs-caculator-wrapper .slider .interest .range');
    var periods_slider = $('.fs-caculator-wrapper .slider .periods .range');
    var deposit_slider = $('.fs-caculator-wrapper .slider .deposit .range');
    if(price_slider.slider)
    price_slider.slider({
        range: "min",
        animate: "fast",
        min:price_slider.data('min'),
        max:price_slider.data('max'),
        slide:function(e,ui){
            var element = ui.handle;
            var value = ui.value;
            var par = $(element).parents('.price');
            par.find('.value').html(value);
            par.find('.value').digits();
            var wrapper = price_slider.closest('.fs-caculator-wrapper');
            wrapper.find('form.fs-finance-calculator-form input[name="fs_finance_price"]').val(value);
        }
    });
    if(interest_slider.slider)
    interest_slider.slider({
        range: "min",
        animate: "fast",
        min:interest_slider.data('min'),
        max:interest_slider.data('max'),
        slide:function(e,ui){
            var element = ui.handle;
            var value = ui.value;
            var par = $(element).parents('.interest');
            par.find('.value').html(value);
            par.find('.value').digits();
            var wrapper = price_slider.closest('.fs-caculator-wrapper');
            wrapper.find('form.fs-finance-calculator-form input[name="fs_finance_rate"]').val(value);
        }
    });
    if(periods_slider.slider)
    periods_slider.slider({
        range: "min",
        animate: "fast",
        min:periods_slider.data('min'),
        max:periods_slider.data('max'),
        slide:function(e,ui){
            var element = ui.handle;
            var value = ui.value;
            var par = $(element).parents('.periods');
            par.find('.value').html(value);
            par.find('.value').digits();
            if(value == 1){
                par.find('.init').html(par.find('.init').data('month'));
            }else{
                par.find('.init').html(par.find('.init').data('months'));                
            }
            var wrapper = price_slider.parents('.fs-caculator-wrapper');        
            wrapper.find('.detail .periods').html(value);
            wrapper.find('form.fs-finance-calculator-form input[name="fs_finance_periods"]').val(value);
        }
    });
    if(deposit_slider.slider)
    deposit_slider.slider({
        range: "min",
        animate: "fast",
        min:deposit_slider.data('min'),
        max:deposit_slider.data('max'),
        slide:function(e,ui){
            var element = ui.handle;
            var value = ui.value;
            var par = $(element).parents('.deposit');
            if(value > price_slider.slider('value')){
                return false;
            }
            par.find('.value').html(value);
            par.find('.value').digits();
            var wrapper = price_slider.parents('.fs-caculator-wrapper');
            wrapper.find('form.fs-finance-calculator-form input[name="fs_finance_deposit"]').val(value);
        }
    });
    var form = $('form.fs-finance-calculator-form');
    if (form.validate) {
        form.validate({
            rules: {
                fs_finance_deposit: {
                    max: function() {
                        return parseFloat(form.find('input[name="fs_finance_price"]').val());
                    }
                }
            }
        });
    }
    form.on('submit', function(e) {
        e.preventDefault();
        // var id = $(this).attr('id');
        if ($(this).valid && !$(this).valid()) {
            return false;
        }
        var price = $(this).find('input[name="fs_finance_price"]').val();
        var rate = $(this).find('input[name="fs_finance_rate"]').val();
        var periods = $(this).find('input[name="fs_finance_periods"]').val();
        var deposit = $(this).find('input[name="fs_finance_deposit"]').val();
        var result = caculate(price, rate, periods, deposit);
        $(this).find('.fs-monthly-payment').html(result.monthlypay.toFixed(2)).digits();
        $(this).find('.fs-total-interest').html(result.interest.toFixed(2)).digits();
        $(this).find('.fs-total-amount').html(result.totalpay.toFixed(2)).digits();
    
    });
});