(function ($) {
    "use strict";
    $.fn.digits = function () {
        return this.each(function () {
            $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
    };
    var fss_init = {
        init: function () {
            // if (typeof fs_filter_data !== 'undefined' || typeof fs_filter_date !== 'undefined' || typeof fs_filter_hp !== 'undefined') {
            //     this.handler.fs_slider();
            //     this.handler.fs_set_position(fs_filter_data.length, '');
            //     this.handler.fs_filter();
            //     this.eventDefine.clickDot();
            // }
            // this.handler.fs_window_resize();
            this.eventDefine.changeIconSelectSearch();
            this.eventDefine.submitSortBarForm();
            this.eventDefine.submitRefineForm();
            this.eventDefine.changeValueFacturer();
            this.eventDefine.resetAllRefineData();
            this.eventDefine.showSlider();
            this.eventDefine.changeLetterBrandSearch();
            this.handler.handlerDataFormSearch();
            this.eventDefine.submitSearch();
            this.eventDefine.showRangeMileage();
        },
        handler: {
            // fs_window_resize: function () {
            //     $(window).resize(function () {
            //         if (typeof fs_filter_data !== 'undefined' || typeof fs_filter_date !== 'undefined' || typeof fs_filter_hp !== 'undefined') {
            //             var _item_height = $(".fs-vehicle-grid").height();
            //             $(".fs-last-car-wrapper").height((_item_height + 14) * 2);
            //             var _this_tab = $(".fs-filter-active");
            //             fss_init.handler.fs_filter_by_class(_this_tab);
            //         }
            //     });
            // },
            // fs_slider: function () {
            //     var _wrapper = $(".fs-last-car-wrapper");
            //     var _item_height = $(".fs-vehicle-grid").height();
            //     var _this_position = 0;
            //     var _big_w;
            //     if (typeof fs_filter_data === 'undefined') {
            //         return;
            //     }
            //     var _datas = fs_filter_data;
            //     var _car_contents = $(".fs-latest-car-content");
            //     var offset_left = _car_contents.position().left;
            //     var _row = ($(".item").data('row'));
            //     var _number_column = Math.round(_datas.length / _row);
            //     if (_number_column < 4) {
            //         _number_column = 4;
            //     }
            //     if (_datas.length <= 4 || _row === 1) {
            //         _wrapper.css("height", _item_height + 14);
            //     } else {
            //         _wrapper.css("height", (_item_height + 14) * 2);
            //     }
            //     var window_size = $(window).width();
            //     var _column = _wrapper.width();
            //     var _w;
            //     if (window_size <= 767) {
            //         _w = _column / 1;
            //     }
            //     if (768 <= window_size && window_size <= 991) {
            //         _w = _column / 2;
            //     }
            //     if (992 <= window_size && window_size <= 1200) {
            //         _w = _column / 3;
            //     }
            //     if (window_size > 1200) {
            //         _w = _column / 4;
            //     }
            //     $(".item").width(_w);
            //     _car_contents.width(_number_column * _w);
            //     //show dot
            //     // fss_init.handler.showDotActive((_car_contents.width() - _column) * -1, (_column * -1), _this_position);
            //     _car_contents.draggable({
            //         revert: false,
            //         revertDuration: 0,
            //         refreshPositions: true,
            //         axis: "x",
            //         start: function (event, ui) {
            //             // _car_contents.css('transition', '');
            //         },
            //         drag: function (event, ui) {
            //             var _left = ui.position.left;
            //             // _car_contents.css('transform', 'translate3d(0px, 0px, 0px)');
            //             var _after_limit_w = parseInt(_car_contents.width());
            //             if (_left > 200) {
            //                 _car_contents.css('transform', 'translate3d(0px, 0px, 0px)');
            //                 // _car_contents.css('left', '0px');
            //                 offset_left = 0;
            //                 _this_position = 0;
            //                 return false;
            //             }
            //             var ok = (_after_limit_w - _column) * -1;
            //             if (_left < (ok - 200)) {
            //                 _car_contents.css('transform', 'translate3d(0px, 0px, 0px)');
            //                 // _car_contents.css('transition', '0.5s all ease 0s');
            //                 // _car_contents.css('left', ok + 'px');
            //                 offset_left = ok;
            //                 _this_position = ok;
            //                 return false;
            //             }
            //         },
            //         stop: function (event, ui) {
            //             var _after_limit_w = parseInt(_car_contents.width());
            //             _column = $(".fs-last-car-wrapper").width();
            //             var ok = (_after_limit_w - _column) * -1;
            //             var _left = ui.position.left;
            //             if (_left > 0) {
            //                 _car_contents.css('transition', '0.25s all ease 0s');
            //                 _car_contents.css('left', '0px');
            //                 offset_left = 0;
            //                 _left = 0;
            //             }
            //             if (_left < (ok)) {
            //                 _car_contents.css('transition', '0.25s all ease 0s');
            //                 _car_contents.css('left', ok + 'px');
            //                 offset_left = ok;
            //                 _left = ok;
            //             }
            //             for (var i = ok; i < 0; i += _w) {
            //                 if (i < _left && _left < (i + _w)) {
            //                     if (_left > offset_left) {
            //                         var _new_left = i + _w;
            //                         _car_contents.css('transition', '0.25s all ease 0s');
            //                         _car_contents.css('left', _new_left + 'px');
            //                         _this_position = _new_left;
            //                     }
            //                     if (_left < offset_left) {
            //                         _car_contents.css('transition', '0.25s all ease 0s');
            //                         _car_contents.css('left', i + 'px');
            //                         _this_position = i;
            //                     }
            //
            //                 }
            //             }
            //             fss_init.handler.showDotActive(ok, (_column * -1), _this_position);
            //             offset_left = _left;
            //         }
            //     });
            // },
            // fs_filter: function () {
            //     var _big_w;
            //     var _show_w;
            //     var _this_pos = 0;
            //     if (typeof fs_filter_data === 'undefined') {
            //         return;
            //     }
            //     $(document).on('click', '.fs-filter-item', function (e) {
            //         var _item = $('.item');
            //         e.preventDefault();
            //         var _this = $(this);
            //         fss_init.handler.fs_filter_by_class(_this);
            //         _item.removeClass(".item-loading");
            //
            //     });
            // },
            // fs_filter_by_class: function (_class) {
            //     var _datas = fs_filter_data;
            //     var _this = _class;
            //     var _wrapper = $(".fs-last-car-wrapper");
            //     var _item_height = $(".fs-vehicle-grid").height();
            //     _wrapper.height((_item_height + 14) * 2);
            //     var _item = $('.item');
            //     var _row = (_item.data('row'));
            //     _item.addClass(".item-loading");
            //     $('.fs-filter-item').removeClass('fs-filter-active');
            //     _this.addClass('fs-filter-active');
            //     var _car_contents = $(".fs-latest-car-content");
            //     _car_contents.css('left', '0px');
            //     var _key = _this.data('filter');
            //     if (_key === "*") {
            //         var _num_col = Math.round(_datas.length / _item.data('row'));
            //         if (_num_col < 4) {
            //             _num_col = 4;
            //         }
            //         if (_datas.length <= 4 || _row === 1) {
            //             _wrapper.css("height", _wrapper.height() / 2);
            //         } else {
            //             _wrapper.css("height", _wrapper.height());
            //         }
            //         var _column = _wrapper.width();
            //         var _w = _column / 4;
            //         _car_contents.width(_num_col * _w);
            //         fss_init.handler.fs_set_position(_datas.length, '');
            //         _item.css("transform", "scale3d(1,1,1)");
            //     } else {
            //         if (_key.search(".filter-") !== -1) {
            //             var _class = _key.replace("filter-", '');
            //             var _num_col = Math.round($(_class).length / _item.data('row'));
            //             if (_num_col < 4) {
            //                 _num_col = 4;
            //             }
            //             if ($(_class).length <= 4 || _row === 1) {
            //                 _wrapper.css("height", _wrapper.height() / 2);
            //             } else {
            //                 _wrapper.css("height", _wrapper.height());
            //             }
            //             var _column = _wrapper.width();
            //             var _w = _column / 4;
            //             _car_contents.width(_num_col * _w);
            //             _item.css("transform", "scale3d(0,0,0)");
            //             fss_init.handler.fs_set_position($(_class).length, _class);
            //             $(_class).css("transform", "scale3d(1,1,1)");
            //         }
            //         if (_key.search(".sort-") !== -1) {
            //             _item.css("transform", "scale3d(1,1,1)");
            //             var _class = _key.replace("sort-", '');
            //             if (_class === ".recently") {
            //                 fss_init.handler.fs_set_position_data(fs_filter_date);
            //             }
            //             if (_class === ".hightper") {
            //                 fss_init.handler.fs_set_position_data(fs_filter_hp);
            //             }
            //         }
            //
            //     }
            // },
            // fs_set_position: function (length_data, _class) {
            //     if (typeof fs_filter_data === 'undefined') {
            //         return;
            //     }
            //     var last_car_wrapper = $(".fs-last-car-wrapper");
            //     var _item_height = $(".fs-vehicle-grid").height();
            //     last_car_wrapper.height((_item_height + 14) * 2);
            //     var _item = $('.item');
            //     var _row = (_item.data('row'));
            //     var _top = last_car_wrapper.height() / _row;
            //     var window_size = $(window).width();
            //     var _wrapper_w = last_car_wrapper.width();
            //     var _width;
            //     var _column;
            //     if (window_size <= 767) {
            //         _width = _wrapper_w / 1;
            //         _column = 1;
            //     }
            //     if (768 <= window_size && window_size <= 991) {
            //         _width = _wrapper_w / 2;
            //         _column = 2;
            //     }
            //     if (992 <= window_size && window_size <= 1200) {
            //         _width = _wrapper_w / 3;
            //         _column = 3;
            //     }
            //     if (window_size > 1200) {
            //         _width = _wrapper_w / 4;
            //         _column = 4;
            //     }
            //     _item.width(_width);
            //     var _number_column = Math.round(length_data / _row);
            //     if (_number_column < _column) {
            //         _number_column = _column;
            //     }
            //     var _big_w = _number_column * _item.width() - last_car_wrapper.width();
            //     fss_init.handler.showDotActive(_big_w * -1, last_car_wrapper.width() * -1, 0);
            //     if (length_data <= 4 || _row === 1) {
            //         last_car_wrapper.css("height", last_car_wrapper.height() / 2);
            //     } else {
            //         last_car_wrapper.css("height", last_car_wrapper.height());
            //     }
            //     if (_class !== "") {
            //         _item = $(_class);
            //     }
            //     $(".fs-latest-car-content").width(_number_column * _width);
            //     _item.each(function (index) {
            //         if (index < _number_column) {
            //             var _left = index * _width;
            //             $(this).css("position", "absolute");
            //             $(this).css("top", "0px");
            //             $(this).css("left", _left + "px");
            //         }
            //         else {
            //             var _left = (index - _number_column) * _width;
            //             $(this).css("position", "absolute");
            //             $(this).css("top", _top + "px");
            //             $(this).css("left", +_left + "px");
            //         }
            //     });
            // },
            // fs_set_position_data: function (datas_array, _class) {
            //     var last_car_wrapper = $(".fs-last-car-wrapper");
            //     var _item_height = $(".fs-vehicle-grid").height();
            //     last_car_wrapper.height((_item_height + 14) * 2);
            //     var _item = $('.item');
            //     var _row = (_item.data('row'));
            //     var _top = last_car_wrapper.height() / _row;
            //     var _number_column = Math.round(datas_array.length / _row);
            //     var window_size = $(window).width();
            //     var _wrapper_w = last_car_wrapper.width();
            //     var _width;
            //     var _column;
            //     if (window_size <= 767) {
            //         _width = _wrapper_w / 1;
            //         _column = 1;
            //     }
            //     if (768 <= window_size && window_size <= 991) {
            //         _width = _wrapper_w / 2;
            //         _column = 2;
            //     }
            //     if (992 <= window_size && window_size <= 1200) {
            //         _width = _wrapper_w / 3;
            //         _column = 3;
            //     }
            //     if (window_size > 1200) {
            //         _width = _wrapper_w / 4;
            //         _column = 4;
            //     }
            //     _item.width(_width);
            //     if (_number_column < _column) {
            //         _number_column = _column;
            //     }
            //     $(".fs-latest-car-content").width(_number_column * _width);
            //     var _big_w = _number_column * _width - last_car_wrapper.width();
            //     fss_init.handler.showDotActive(_big_w * -1, last_car_wrapper.width() * -1, 0);
            //     if (datas_array.length <= 4 || _row === 1) {
            //         last_car_wrapper.css("height", last_car_wrapper.height() / 2);
            //     } else {
            //         last_car_wrapper.css("height", last_car_wrapper.height());
            //     }
            //     if (_class !== "") {
            //         _item = $(_class);
            //     }
            //     for (var i = 0; i < datas_array.length; i++) {
            //         var index = datas_array[i].index;
            //         var _this = $('.item').eq(index);
            //         if (i < _number_column) {
            //             var _left = i * _width;
            //             _this.css("position", "absolute");
            //             _this.css("top", "0px");
            //             _this.css("left", _left + "px");
            //         }
            //         else {
            //             var _left = (i - _number_column) * _width;
            //             _this.css("position", "absolute");
            //             _this.css("top", _top + "px");
            //             _this.css("left", +_left + "px");
            //         }
            //     }
            // },
            // showDotActive: function (big_w, show_w, position) {
            //     var _dot_list = $(".fs-latest-controls");
            //     _dot_list.html("");
            //     for (var j = 0; j > big_w + show_w; j += show_w) {
            //         var old_html = _dot_list.html();
            //         var _active_dot = "";
            //         if (position > (j + show_w) && position <= j) {
            //             _active_dot = "fs_dot_active";
            //         }
            //         var _x = j + show_w;
            //         _dot_list.html(old_html + "<span class='fs-latest-dot " + _active_dot + "'></span>");
            //     }
            // },
            renderPriceSlider: function () {
                //price slider
                var _price = $(".fss-filter-price");
                var price_input = $(".fss-refine-price");
                var price_val_arr = [];
                if (price_input.length < 1) {
                    return;
                }
                if (price_input.val() !== '') {
                    price_val_arr = price_input.val().split(",");
                }
                else {
                    var _pri = _price.data('min') + "," + _price.data('max');
                    price_val_arr = _pri.split(",");
                }
                // $("#slider").on("valuesChanging", function(e, data){
                //     console.log("Something moved. min: " + data.values.min + " max: " + data.values.max);
                // });
                $("#fs_price_slider").rangeSlider({
                        bounds: {min: _price.data('min'), max: _price.data('max')},
                        defaultValues: {min: price_val_arr[0], max: price_val_arr[1]}
                    }
                ).bind("valuesChanged", function (e, data) {
                    $('.fs_price_from').html(Math.round(data.values.min));
                    price_input.attr('data-price', Math.round(data.values.min) + "," + Math.round(data.values.max));
                    $('.fs_price_to').html(Math.round(data.values.max));
                });
            },
            renderYearSlider: function () {
                //year slider
                var _year = $(".fss-filter-year");
                var year_input = $(".fss-refine-year");
                var year_val_arr = [];
                if (year_input.length < 1) {
                    return;
                }
                if (year_input.val() !== '') {
                    year_val_arr = year_input.val().split(",");
                }
                else {
                    var y = _year.data('min') + "," + new Date().getFullYear();
                    year_val_arr = y.split(",");
                }
                $("#fs_year_slider").rangeSlider({
                        bounds: {min: _year.data("min"), max: new Date().getFullYear()},
                        defaultValues: {min: year_val_arr[0], max: year_val_arr[1]}
                    }
                ).bind("valuesChanged", function (e, data) {
                    $('.fs_year_from').html(Math.round(data.values.min));
                    $('.fs_year_to').html(Math.round(data.values.max));
                    year_input.attr('data-year', Math.round(data.values.min) + "," + Math.round(data.values.max));
                });
            },
            renderMileageSlider: function () {
                //mileage slider
                var _mileage = $(".fss-filter-mileage");
                var mileage_input = $(".fss-refine-mileage");
                var mileage_val_arr = [];
                if (mileage_input.length < 1) {
                    return;
                }
                if (mileage_input.val() !== '') {
                    mileage_val_arr = mileage_input.val().split(",");
                }
                else {
                    var _mile = _mileage.data('min') + "," + _mileage.data('max');
                    mileage_val_arr = _mile.split(",");
                }
                $("#fs_mileage_slider").rangeSlider({
                        bounds: {min: _mileage.data("min"), max: _mileage.data("max")},
                        defaultValues: {min: mileage_val_arr[0], max: mileage_val_arr[1]}
                        // range: {min: 1, max: 400}
                    }
                ).bind("valuesChanged", function (e, data) {
                    $('.fs_mileage_from').html(Math.round(data.values.min));
                    $('.fs_mileage_to').html(Math.round(data.values.max));
                    mileage_input.attr('data-mileage', Math.round(data.values.min) + "," + Math.round(data.values.max));
                });
            },
            changeValueRefineToSort: function () {
                //get value from refineForm
                var manufacturer = $('.fss-refine-manufacturer').val();
                var make = $('.fss-refine-make').val();
                var price = $('.fss-refine-price').val();
                var year = $('.fss-refine-year').val();
                var milage = $('.fss-refine-mileage').val();
                var fuel = $('.fss-refine-fuel').val();
                var gearbox = $('.fss-refine-gearbox').val();
                var door = $('.fss-refine-door').val();
                var color = $('.fss-refine-colour').val();
                var seller_type = $('.fss-refine-seller-type').val();
                //set value to sortBarForm
                $('.fss-refine-manufacturer-extend').val(manufacturer);
                $('.fss-refine-make-extend').val(make);
                $('.fss-refine-price-extend').val(price);
                $('.fss-refine-year-extend').val(year);
                $('.fss-refine-mileage-extend').val(milage);
                $('.fss-refine-fuel-extend').val(fuel);
                $('.fss-refine-gearbox-extend').val(gearbox);
                $('.fss-refine-door-extend').val(door);
                $('.fss-refine-colour-extend').val(color);
                $('.fss-refine-seller-type-extend').val(seller_type);
            },
            changeValueSortToRefine: function () {
                //get value from sortBarForm
                var sort_by = $('.fs_sort_by_select').val();
                var per_page = $('.fs_per_page_select').val();
                var sort_layout = $('input[name=sort_layout]:checked').val();
                //set value to RefineForm
                $('.fs_sort_by_extend').val(sort_by);
                $('.fs_per_page_extend').val(per_page);
                $('.sort_layout_extend').val(sort_layout);
            },
            handlerDataFormSearch: function () {
                $(document).on('change', '.fs-price-min', function () {
                    var _min_val = $(this).val();
                    if (_min_val === "") {
                        _min_val = 0;
                    } else {
                        _min_val = parseInt($(this).val());
                    }
                    $('.fs-price-max').find('option').each(function () {
                        if (!(parseInt($(this).val()) > _min_val)) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
                $(document).on('change', '.fs-price-max', function () {
                    var _max_val = $(this).val();
                    if (_max_val === "") {
                        _max_val = 0;
                    } else {
                        _max_val = parseInt($(this).val());
                    }
                    $('.fs-price-min').find('option').each(function () {
                        if (parseInt($(this).val()) >= _max_val && _max_val !== 0) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
                $(document).on('change', '.fs-mileage-min', function () {
                    var _min_val = $(this).val();
                    if (_min_val === "") {
                        _min_val = 0;
                    } else {
                        _min_val = parseInt($(this).val());
                    }
                    $('.fs-mileage-max').find('option').each(function () {
                        if (!(parseInt($(this).val()) > _min_val)) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
                $(document).on('change', '.fs-mileage-max', function () {
                    var _max_val = $(this).val();
                    if (_max_val === "") {
                        _max_val = 0;
                    } else {
                        _max_val = parseInt($(this).val());
                    }
                    $('.fs-mileage-min').find('option').each(function () {
                        if (parseInt($(this).val()) >= _max_val && _max_val !== 0) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
                $(document).on('change', '.fs-year-min', function () {
                    var _min_val = $(this).val();
                    if (_min_val === "") {
                        _min_val = 0;
                    } else {
                        _min_val = parseInt($(this).val());
                    }
                    $('.fs-year-max').find('option').each(function () {
                        if (!(parseInt($(this).val()) > _min_val)) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
                $(document).on('change', '.fs-year-max', function () {
                    var _max_val = $(this).val();
                    if (_max_val === "") {
                        _max_val = 0;
                    } else {
                        _max_val = parseInt($(this).val());
                    }
                    $('.fs-year-min').find('option').each(function () {
                        if (parseInt($(this).val()) >= _max_val && _max_val !== 0) {
                            $(this).attr('disabled', true);
                        }
                        else {
                            $(this).removeAttr('disabled');
                        }
                    });
                });
            }
        },
        eventDefine: {
            changeIconSelectSearch: function () {
                $(document).on('focusout', ".fs-search-form-wrap select", function () {
                    $(this).next().attr("class", "zmdi zmdi-chevron-down");
                });
                // $(document).on('focusin', ".fs-search-form-wrap select", function () {
                //     $(this).next().attr("class", "zmdi zmdi-chevron-up");
                // });
                $(document).on('change', ".fs-search-form-wrap select", function () {
                    $(this).next().attr("class", "zmdi zmdi-chevron-down");
                });
                $(document).on('click', ".fs-search-form-wrap select", function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    if (_this.hasClass("fs-select-active")) {
                        _this.removeClass("fs-select-active");
                        _this.next().attr("class", "zmdi zmdi-chevron-down");

                    } else {
                        _this.addClass("fs-select-active");
                        _this.next().attr("class", "zmdi zmdi-chevron-up");
                    }
                });
            },
            showRangeMileage: function () {
                var max_value = $(".fs-search-mileage").attr("max-value");
                var _range = $(".fss-range-mileage");
                _range.slider({
                    slide: function (event, ui) {
                        var input_postcode = $("#fsm_postcode");
                        if (input_postcode.val() === "") {
                            input_postcode.focus();
                            $(".fs-search-mileage").addClass("active-tooltip");
                            return false;
                        }
                        else {
                            $(".fs-search-mileage").removeClass("active-tooltip");
                            $(".fss-value-range").html(ui.value).digits();
                            var input_mile = $(".fss-refine-distance");
                            if (ui.value === 0) {
                                input_mile.val("");
                            } else {
                                input_mile.val(ui.value);
                            }
                        }
                    },
                    max: 10000
                });
                $(document).on('input','.fsm_postcode',function () {
                    var _val = $(this).val();
                    var _char_allow = ['-'];
                    var _arr = _val.split('');
                    var _result_val = "";
                    _arr.forEach(function (item) {
                        if (!isNaN(item))
                        {
                            _result_val+=item;
                            return;
                        }
                        if(_char_allow.indexOf(item) !== -1){
                            _result_val+=item;
                            return;
                        }
                    });
                    $(this).val(_result_val);
                });
            },
            showSlider: function () {
                //show price slider
                $(document).on('click', '.fss-toggle-price', function (e) {
                    e.preventDefault();
                    fss_init.handler.renderPriceSlider();
                });
                if ($('.fss-toggle-price').hasClass("open")) {
                    fss_init.handler.renderPriceSlider();
                }

                //show year slider
                $(document).on('click', '.fss-toggle-year', function (e) {
                    e.preventDefault();
                    fss_init.handler.renderYearSlider();
                });
                if ($('.fss-toggle-year').hasClass("open")) {
                    fss_init.handler.renderYearSlider();
                }

                //show mileage slider
                $(document).on('click', '.fss-toggle-mileage', function (e) {
                    e.preventDefault();
                    fss_init.handler.renderMileageSlider();
                });

                if ($('.fss-toggle-mileage').hasClass("open")) {
                    fss_init.handler.renderMileageSlider();
                }
            },
            submitSortBarForm: function () {
                var sort_bar_form = $(".fs-sort-bar");
                //sort bar handler
                var current_url = window.location.protocol + "//" + window.location.host + window.location.pathname;

                function convert_query(queries) {
                    if (history.pushState) {
                        var str = "?";
                        for (var query in queries) {
                            str += query + '=' + queries[query] + '&';
                        }
                        str = str.substr(0, (str.length - 1));
                        window.history.pushState({
                            path: current_url + str
                        }, '', current_url + str);
                    }
                }
                var queries = {};
                var querystring = decodeURI(document.location.search);
                if (querystring.length > 0) {
                    $.each(querystring.substr(1).split('&'), function(c, q) {
                        var i = q.split('=');
                        if(i.length >=2 ){
                            queries[i[0].toString()] = i[1].toString();
                        }
                    })
                };

                $(document).on('change','form[name="fs_sort_by"] input,form[name="fs_sort_by"] select',function(e){
                    e.preventDefault();
                    queries[this.name] = this.value;
                    convert_query(queries);
                    location.reload();
                    // return false;
                    // if(link.indexOf('?') == -1){
                    //     window.location = link+'?'+this.name+'='+this.value;
                    // }else{
                    //     window.location = link+'&'+this.name+'='+this.value;
                    // }
                });
                $('.fs-sort-by-select').change(function () {
                    fss_init.handler.changeValueRefineToSort();
                    // sort_bar_form.submit();
                });
                $(".fs-sort-layout").change(function () {
                    fss_init.handler.changeValueRefineToSort();
                    // sort_bar_form.submit();
                });
                $(".fs-per-page-select").change(function () {
                    fss_init.handler.changeValueRefineToSort();
                    // sort_bar_form.submit();
                });
            },
            submitRefineForm: function () {
                var refine_form = $('.fss-refine-form');
                $('.fss-refine-make').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });
                $(document).on('click', '.fss-filter-price-btn', function (e) {
                    e.preventDefault();
                    fss_init.handler.changeValueSortToRefine();
                    var _value_pri = $(".fss-refine-price").attr("data-price");
                    $(".fss-refine-price").val(_value_pri);
                    refine_form.submit();
                });
                $(document).on('click', '.fss-filter-year-btn', function (e) {
                    e.preventDefault();
                    fss_init.handler.changeValueSortToRefine();
                    var _value_year = $(".fss-refine-year").attr("data-year");
                    $(".fss-refine-year").val(_value_year);
                    refine_form.submit();
                });
                $(document).on('click', '.fss-filter-mileage-btn', function (e) {
                    e.preventDefault();
                    fss_init.handler.changeValueSortToRefine();
                    var _value_mileage = $(".fss-refine-mileage").attr("data-mileage");
                    $(".fss-refine-mileage").val(_value_mileage);
                    refine_form.submit();
                });
                $('.fss-refine-fuel').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });
                $('.fss-refine-gearbox').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });
                $('.fss-refine-door').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });
                $('.fss-refine-colour').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });
                $('.fss-refine-seller-type').change(function () {
                    fss_init.handler.changeValueSortToRefine();
                    refine_form.submit();
                });

            },
            changeValueFacturer: function () {
                $('.fss-refine-manufacturer').change(function () {
                    var manu_id = $(this).val();
                    var make_select = $('.fss-refine-make');
                    $.ajax({
                        url: fss_var.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            make_select.attr('disabled', 'disabled');
                            make_select.addClass('fs-busy');
                        },
                        data: {action: 'fss_change_manufacturer', manu_id: manu_id}
                    })
                        .done(function (data) {
                            var layout_make = "<option value=''>All make</option>";
                            for (var i = 0; i < data.length; i++) {
                                layout_make += "<option value='" + data[i].term_id + "'>" + data[i].name + "</option>";
                            }
                            make_select.html(layout_make);
                            var refine_form = $('.fss-refine-form');
                            fss_init.handler.changeValueSortToRefine();
                            refine_form.submit();
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            make_select.removeClass('fs-busy');
                            make_select.removeAttr('disabled');
                        });
                });
            },
            resetAllRefineData: function () {
                $(document).on('click', '.fss-btn-reset', function (e) {
                    e.preventDefault();
                    $('.fss-refine-manufacturer').val("");
                    $('.fss-refine-make').val("");
                    $('.fss-refine-price').val("");
                    $('.fss-refine-year').val("");
                    $('.fss-refine-mileage').val("");
                    $('.fss-refine-fuel').val("");
                    $('.fss-refine-gearbox').val("");
                    $('.fss-refine-door').val("");
                    $('.fss-refine-colour').val("");
                    $('.fss-refine-seller-type').val("");
                    fss_init.handler.changeValueSortToRefine();
                    $('.fss-refine-form').submit();
                });
            },
            changeLetterBrandSearch: function () {
                $(document).on('click', '.fs-brand-search-letter', function (e) {
                    e.preventDefault();
                    var _letter = $(this).attr('fs-letter');
                    if (_letter === "all") {
                        $('.fs-brand-search-item').removeClass('fs-hidden');
                        $('.fs-brand-search-letter').removeClass('fs-active');
                        $('.fs-brand-head-all').addClass('fs-active');
                    } else {
                        $('.fs-brand-search-item').addClass('fs-hidden');
                        $('.fs-brand-item-' + _letter).removeClass('fs-hidden');
                        $('.fs-brand-search-letter').removeClass('fs-active');
                        $('.fs-brand-head-' + _letter).addClass('fs-active');
                    }
                });


            },
            submitSearch: function () {
                $(document).on('click', '.fs-s-btn-submit', function (e) {
                    e.preventDefault();
                    var _price_min = $('.fs-price-min').val();
                    var _price_max = $('.fs-price-max').val();
                    if (_price_min === "" || _price_max === "" || _price_min === null || _price_max === null) {
                        $('.fss-search-price').val("");
                    } else {
                        $('.fss-search-price').val(_price_min + "," + _price_max);
                    }
                    var _mile_min = $('.fs-mileage-min').val();
                    var _mile_max = $('.fs-mileage-max').val();
                    if (typeof _mile_min !== "undefined" && typeof _mile_max !== "undefined") {
                        if (_mile_min === "" || _mile_max === "" || _mile_min === null || _mile_max === null) {
                            $('.fss-refine-mileage').val("");
                        } else {
                            $('.fss-refine-mileage').val(_mile_min + "," + _mile_max);
                        }
                    }

                    var _year_min = $('.fs-year-min').val();
                    var _year_max = $('.fs-year-max').val();
                    if (_year_min === "" || _year_max === "" || _year_min === null || _year_max === null) {
                        $('.fss-refine-year').val("");
                    } else {
                        $('.fss-refine-year').val(_year_min + "," + _year_max);
                    }
                    $('.fs-search-form').submit();
                });
                $(document).on('click', '.fs-find-btn-reset', function (e) {
                    e.preventDefault();
                    $('.fss-refine-manufacturer').val("");
                    $('.fss-refine-make').val("");
                    $('.fs-price-min').val("");
                    $('.fs-price-max').val("");
                    $('.fss-search-price').val("");
                    $('.fs-mileage-min').val("");
                    $('.fs-mileage-max').val("");
                    $('.fss-refine-mileage').val("");
                    $('.fs-year-min').val("");
                    $('.fs-year-max').val("");
                    $('.fss-refine-year').val("");
                    $('.fs-filter-fuel-type').val("");
                    $('.fs-filter-gearbox').val("");
                });
            },
            clickDot: function () {
                $(document).on('click', '.fs-latest-dot', function (e) {
                    e.preventDefault();
                    $(".fs-latest-car-content").css('transition', '0.25s all ease 0s');
                    var _this = $(this);
                    if (!_this.hasClass("fs_dot_active")) {
                        $('.fs-latest-dot').removeClass('fs_dot_active');
                        _this.addClass('fs_dot_active');
                        var _index = _this.index();
                        var _car_contents = $(".fs-latest-car-content");
                        var _show_contents = $(".fs-last-car-wrapper");
                        var _pos = _show_contents.width() * _index * -1;
                        if (_pos <= (_car_contents.width() - _show_contents.width()) * -1) {
                            _pos = (_car_contents.width() - _show_contents.width()) * -1;
                        }
                        _car_contents.css('left', _pos + 'px');
                    }
                });
            }
        }

    };
    fss_init.init();
})(jQuery);
// jQuery(window).load(function (){
//     var _wrap = jQuery(".fs-last-car-wrapper");
//     var _item_height = jQuery(".fs-vehicle-grid").height();
//     var _row = (jQuery(".item").attr("data-row"));
//     if (parseInt(_row) === 1) {
//         _wrap.height((_item_height + 14));
//     } else {
//         _wrap.height((_item_height + 14) * 2);
//     }
// });