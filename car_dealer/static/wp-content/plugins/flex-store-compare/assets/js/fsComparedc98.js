/**
 * @team: FsFlex Team
 * @since: 1.0.0
 * @author: KP
 */
(function ($) {
    var fs_compare_init = {
            init: function () {
                this.eventsDefine.addCompare();
                this.eventsDefine.removeCompare();

            },
            handlers: {
                actionAddCompare: function (id,_this) {
                    var product_id = id;
                    $.ajax({
                        url: fs_compare_var.ajax_url,
                        async: false,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'fs_compare_add_to_list',
                            fs_product_id_cp: product_id
                        }
                    })
                        .done(function (data) {
                            var _remove_btn = _this.parent().find('.fs-remove-com-' + product_id);
                            if (data.status === "done") {
                                _this.parent().find('.fs-add-com-' + product_id).css('display', 'none');
                                _remove_btn.css('display', 'inline-block');
                            }
                            _remove_btn.parent().append(data.noti);
                            setTimeout(function () {
                                var _noti = $(".fs-notification-" + product_id);
                                if (_noti.length >= 1) {
                                    _noti.remove();
                                }
                            }, 5000);
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                },
                actionRemoveCompare: function (id,_this) {
                    var product_id = id;
                    $.ajax({
                        url: fs_compare_var.ajax_url,
                        async: false,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'fs_compare_remove_from_list',
                            fs_product_id_cp: product_id
                        }
                    })
                        .done(function (data) {
                            var _remove_btn = _this.parent().find('.fs-remove-com-' + product_id);
                            if (data.status === "done") {
                                _remove_btn.css('display', 'none');
                                _this.parent().find('.fs-add-com-' + product_id).css('display', 'inline-block');
                            }
                            _remove_btn.parent().append(data.noti);
                            setTimeout(function () {
                                var _noti = $(".fs-notification-" + product_id);
                                if (_noti.length >= 1) {
                                    _noti.remove();
                                }
                            }, 5000);
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                }
            },
            eventsDefine: {
                addCompare: function () {
                    $(document).on('click', '.fs-add-to-compare', function (e) {
                        e.preventDefault();
                        var _this = $(this),
                        product_id = $(this).attr("data-productId");
                        fs_compare_init.handlers.actionAddCompare(product_id,_this);
                    });

                },
                removeCompare: function () {
                    $(document).on('click', '.fs-remove-compare', function (e) {
                        e.preventDefault();
                        var _this = $(this);
                        var product_id = $(this).attr("data-productId");
                        fs_compare_init.handlers.actionRemoveCompare(product_id,_this);
                    });
                    $(document).on('click', '.fs-compare-page-remove', function (e) {
                        var _this = $(this);
                        e.preventDefault();
                        var product_id = $(this).attr("data-productId");
                        $.ajax({
                            url: fs_compare_var.ajax_url,
                            async: false,
                            type: 'POST',
                            beforeSend: function () {
                            },
                            data: {
                                action: 'fs_compare_remove_in_page',
                                fs_product_id_cp: product_id
                            }
                        })
                            .done(function (data) {
                                var _compare_table = $('.fs-compare-name');
                                if (data.status === "failed") {
                                    _compare_table.parent().append(data.noti);
                                    setTimeout(function () {
                                        var _noti = $(".fs-notification-"+product_id);
                                        if (_noti.length >= 1) {
                                            _noti.remove();
                                        }
                                    }, 5000);
                                }
                                if (data.status === "done") {
                                    var _key = _this.attr('fsKey');
                                    $('.fs-compare-col-' + _key).remove();
                                    $('.fs-compare-head').append(data.head);
                                    $('.fs-compare-body').append(data.body);
                                    if (data.count === 0) {
                                        $('.fs-compare-feature').css('display', 'none');
                                    }
                                }
                            })
                            .fail(function () {
                                return false;
                            })
                            .always(function () {
                                return false;
                            });
                    });
                }
            }
        }
    ;
    fs_compare_init.init();
})(jQuery);