/**
 * @team: FsFlex Team
 * @since: 1.0.0
 * @author: KP
 */
(function ($) {
    var fs_schedule_init = {
            init: function () {
                this.eventsDefine.showPopup();
            },
            handlers: {},
            eventsDefine: {
                showPopup: function () {
                    $(document).on('click', '.fs-schedule-car', function (e) {
                        $(".fssche-modal").remove();
                        var _this = $(this);
                        var _id = _this.data("fsscheduleid");
                        e.preventDefault();
                        $.ajax({
                            url: fssche_var.ajax_url,
                            type: 'POST',
                            sync: false,
                            beforeSend: function () {
                            },
                            data: {action: 'show_form_schedule', id: _id}
                        })
                            .done(function (data) {
                                _this.after(data);
                                $('#fssche-time').datetimepicker({
                                    minDate: 0
                                });
                                $(document).on('click', '.fssche-modal', function (e) {
                                    e.preventDefault();
                                    var modal = $('.fssche-modal');
                                    if ($(e.target)['0'] === modal[0]) {
                                        modal.remove();
                                    }
                                });
                                fs_schedule_init.eventsDefine.submit(_id);
                            })
                            .fail(function () {
                                return false;
                            })
                            .always(function () {
                            });
                    });
                },
                submit: function (id) {
                    $(document).on('click', '.fssche-btn-submit', function (e) {
                        e.preventDefault();
                        var _name = $(".fssche-name"),
                            _email = $(".fssche-email"),
                            _phone = $(".fssche-phone"),
                            _time = $(".fssche-time"),
                            _check = {
                                'name': _name,
                                'email': _email,
                                'phone': _phone,
                                'time': _time
                            },
                            _check_val = true;
                        $.each(_check, function () {
                            $(this).removeClass("fss-error");
                            if ($(this).val() === "" || $(this).val() === null) {
                                $(this).addClass("fss-error");
                                _check_val = false;
                            }
                        });
                        if (_check.email.length > 0) {
                            var atpos = _check.email.val().indexOf("@");
                            var dotpos = _check.email.val().lastIndexOf(".");
                            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= _check.email.val().length) {
                                _check.email.addClass('fss-error');
                                _check_val = false;
                            }
                        }
                        if (_check_val === true) {
                            var _wait = $(document).find('.fss-wait');
                            $.ajax({
                                url: fssche_var.ajax_url,
                                type: 'POST',
                                beforeSend: function () {
                                    _wait.show();
                                },
                                data: {
                                    action: 'submit_form_schedule',
                                    fss_name: _check.name.val(),
                                    fssche_email: _check.email.val(),
                                    fssche_phone: _check.phone.val(),
                                    fssche_date_time: _check.time.val(),
                                    fss_content: $(".fssche-car-name").html() + " - " + id
                                }
                            })
                                .done(function (data) {
                                    _wait.hide();
                                    if (typeof data.msg !== "undefined") {
                                        $(".fssche-contents").html("<p>" + data.msg + "</p>");
                                    }
                                })
                                .fail(function () {
                                    return false;
                                })
                                .always(function () {
                                    _wait.hide();
                                });
                        }
                    });
                }
            }
        }
    ;
    fs_schedule_init.init();
})(jQuery);