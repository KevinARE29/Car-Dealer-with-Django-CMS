from django.contrib import admin
from .models import *
from multiupload.admin import MultiUploadAdmin

class CarImageInline(admin.TabularInline):
    model = CarImage
    extra = 3

class CarMultiuploadMixing(object):

    def process_uploaded_file(self, uploaded, car, request):
        if car:
            image = car.images.create(image=uploaded)
        else:
            image = CarImage.objects.create(image=uploaded, car=None)
        return {
            'url': image.image.url,
            'thumbnail_url': image.image.url,
            'id': image.id,
            # 'name': image.filename
        }

class CarAdmin(CarMultiuploadMixing, MultiUploadAdmin):
    inlines = [CarImageInline,]
    multiupload_template = 'upload.html'
    multiupload_form = True
    multiupload_list = False
    multiupload_maxfilesize = 30 * 2 ** 20 # 30 Mb

    def delete_file(self, pk, request):
        '''
        Delete an image.
        '''
        obj = get_object_or_404(CarImage, pk=pk)
        return obj.delete()

# Register your models here.
admin.site.register(Category)
admin.site.register(Feature)
admin.site.register(Manufacturer)
admin.site.register(Car, CarAdmin)
admin.site.register(Manager)
