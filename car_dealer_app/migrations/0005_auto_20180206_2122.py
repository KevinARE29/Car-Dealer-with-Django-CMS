# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('car_dealer_app', '0004_auto_20180201_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='doors',
            field=models.IntegerField(default=4, verbose_name=b'Doors', validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='car',
            name='mileage',
            field=models.DecimalField(default=0, verbose_name=b'Mileage', max_digits=8, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='car',
            name='price',
            field=models.DecimalField(decimal_places=2, validators=[django.core.validators.MinValueValidator(0)], max_digits=8, blank=True, null=True, verbose_name=b'Price'),
        ),
        migrations.AlterField(
            model_name='car',
            name='seats',
            field=models.IntegerField(default=5, verbose_name=b'Seats', validators=[django.core.validators.MinValueValidator(0)]),
        ),
    ]
