# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('year', models.IntegerField(verbose_name=b'Year', validators=[django.core.validators.MinValueValidator(0)])),
                ('price', models.DecimalField(verbose_name=b'Price', max_digits=8, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)])),
                ('doors', models.IntegerField(verbose_name=b'Doors', validators=[django.core.validators.MinValueValidator(0)])),
                ('seats', models.IntegerField(verbose_name=b'Seats', validators=[django.core.validators.MinValueValidator(0)])),
                ('mileage', models.DecimalField(verbose_name=b'Mileage', max_digits=8, decimal_places=2, validators=[django.core.validators.MinValueValidator(0)])),
                ('fuel_type', models.CharField(default=b'1', max_length=1, verbose_name=b'Fuel Type', choices=[(b'1', b'Petrol'), (b'2', b'Diesel'), (b'3', b'Electric')])),
                ('transmission', models.CharField(default=b'1', max_length=1, verbose_name=b'Transmission', choices=[(b'1', b'Automatic'), (b'2', b'Standar')])),
            ],
        ),
        migrations.CreateModel(
            name='CarImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'car_images')),
                ('car', models.ForeignKey(related_name='images', to='car_dealer_app.Car')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'Name')),
                ('image', models.ImageField(upload_to=b'manufacturer_images')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='category',
            field=models.ForeignKey(to='car_dealer_app.Category'),
        ),
        migrations.AddField(
            model_name='car',
            name='features',
            field=models.ManyToManyField(to='car_dealer_app.Feature'),
        ),
        migrations.AddField(
            model_name='car',
            name='manufacturer',
            field=models.ForeignKey(to='car_dealer_app.Manufacturer'),
        ),
    ]
