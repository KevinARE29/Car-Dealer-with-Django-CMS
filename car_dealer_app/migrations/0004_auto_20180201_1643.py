# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car_dealer_app', '0003_manager'),
    ]

    operations = [
        migrations.AddField(
            model_name='manager',
            name='job',
            field=models.CharField(default=1, max_length=40, verbose_name=b'Managers Job'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='manager',
            name='phone_number',
            field=models.CharField(help_text=b'Formato: XXX-XXX-XXXX', unique=True, max_length=12, verbose_name=b'Managers Phone Number'),
        ),
    ]
