# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car_dealer_app', '0002_auto_20180122_2150'),
    ]

    operations = [
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40, verbose_name=b'Managers Name')),
                ('last_name', models.CharField(max_length=40, verbose_name=b'Managers Last Name')),
                ('phone_number', models.CharField(help_text=b'Formato: XXXX-XXXX', unique=True, max_length=9, verbose_name=b'Managers Phone Number')),
                ('email', models.EmailField(max_length=254, unique=True, null=True, verbose_name=b'Managers E-mail', blank=True)),
                ('facebook', models.CharField(max_length=70, verbose_name=b'Managers Facebook')),
                ('twitter', models.CharField(max_length=70, verbose_name=b'Managers Twitter')),
                ('instagram', models.CharField(max_length=70, verbose_name=b'Managers Instagram')),
                ('photo', models.ImageField(upload_to=b'manager_images', verbose_name=b'Photograph')),
            ],
        ),
    ]
