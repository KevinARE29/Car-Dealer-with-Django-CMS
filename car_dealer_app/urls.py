from django.conf.urls import url
from . import views

urlpatterns = [
    url('^$', views.home, name = 'home'),
    url('^contact/$', views.send_email, name = 'contact'),
    url('^car_detail/(?P<pk>\d+)$', views.car_detail, name = 'car_detail'),
    url('^car_list/$', views.car_list, name='car_list'),
]