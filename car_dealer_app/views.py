from django.shortcuts import render
from .models import *
from django.views.generic import TemplateView
from .forms import ContactoForm
from django.core.mail import send_mail
# Create your views here.
def home(request):
    manufacturer_list = Manufacturer.objects.all()
    category_list = Category.objects.all()
    cars = Car.objects.all()
    car_list = []
    for car in cars:
        car_list.append([car, car.images.all(), car.images.all().count()])
    last_three = car_list[:] # Copiando en una lista diferente
    last_three.reverse()
    if car_list:
        last = car_list[-1]
    else:
        last = None
    context = {
        'manufacturer_list': manufacturer_list,
        'category_list': category_list,
        'car_list': car_list,
        'manager': Manager.objects.first(),
        'last': last,
        'last_three': last_three[:3] # Deben haber al menos dos registros para que se muestren en la Template de lo contrario se mostrara vacio el elemento
    }
    return render(request, 'index.html', context)

def car_list(request):
    manufacturer_selected = fuel_type_selected = category_selected = None
    price_min = price_min_selected = MIN_PRICE
    price_max = price_max_selected = MAX_PRICE
    year_min = MIN_YEAR
    year_max = MAX_YEAR
    manufacturer_list = Manufacturer.objects.all()
    category_list = Category.objects.all()
    cars = Car.objects.all()
    if request.method == 'GET':
        if request.GET.get('category'):
            cars = cars.filter(category = request.GET.get('category'))
            category_selected = int(request.GET.get('category'))
        if request.GET.get('manufacturer'):
            cars = cars.filter(manufacturer = request.GET.get('manufacturer'))
            manufacturer_selected = int(request.GET.get('manufacturer'))
    else: #if request.method == 'POST':
        if request.POST.get('fss-refine-manufacturer'):
            manufacturer_selected = int(request.POST.get('fss-refine-manufacturer'))
            cars = cars.filter(manufacturer = manufacturer_selected)
        if request.POST.get('fss-refine-category'):
            category_selected = int(request.POST.get('fss-refine-category'))
            cars = cars.filter(category = category_selected)
        if request.POST.get('fss-refine-fuel_type'):
            fuel_type_selected = request.POST.get('fss-refine-fuel_type')
            cars = cars.filter(fuel_type = fuel_type_selected)
        if request.POST.get('fss-refine-meta_price'):
            price_min_selected = request.POST.get('fss-refine-meta_price').split(',')[0]
            price_max_selected = request.POST.get('fss-refine-meta_price').split(',')[1]
            cars = cars.filter(price__gte = price_min_selected)
            cars = cars.filter(price__lte = price_max_selected)
        if request.POST.get('fss-refine-year'):
            cars = cars.filter(year__gte = request.POST.get('fss-refine-year').split(',')[0])
            cars = cars.filter(year__lte = request.POST.get('fss-refine-year').split(',')[1])

    car_list = []
    for car in cars:
        car_list.append([car, car.images.all(), car.images.all().count()])
    last_three = car_list[:]  # Copiando en una lista diferente
    last_three.reverse()
    context = {
        'manufacturer_list': manufacturer_list,
        'category_list': category_list,
        'car_list': car_list,
        'fuel_type_list': FUEL_TYPE,
        'manufacturer_selected': manufacturer_selected,
        'category_selected': category_selected,
        'fuel_type_selected': fuel_type_selected,
        'price': [price_min, price_max],
        'price_selected': [price_min_selected, price_max_selected],
        'year_min': MIN_YEAR,
        'manager': Manager.objects.first(),
        'year_max': MAX_YEAR,
        'last_three': last_three[:3]
    }
    return render(request, 'ListView.html', context)

def car_detail(request, pk):
    cars = Car.objects.all()
    car_list = []
    for car in cars:
        car_list.append([car, car.images.all(), car.images.all().count()])
    last_three = car_list[:]  # Copiando en una lista diferente
    last_three.reverse()
    car = Car.objects.get(pk=pk)
    context = {
        'car': car,
        'car_images': car.images.all(),
        'features': car.features.all(),
        'manufacturer_list': Manufacturer.objects.all(),
        'manager': Manager.objects.first(),
        'category_list': Category.objects.all(),
        'last_three': last_three[:3]
    }
    return render(request, 'DetailView.html', context)

def send_email(request):
    extra = " "
    if request.method == 'POST':
        form = ContactoForm(request.POST or None)
        if form.is_valid():
            mail = 'cars.dealer.email@gmail.com'
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            name = form.cleaned_data['name']
            phone_number = form.cleaned_data['phone']
            car = form.cleaned_data['car']
            subject = "I'm looking for a car"
            to_email = ['doyouneedacarnow@gmail.com']
            body_message = message + "\n\n\n" + "Type of car:   " + car  + "\n" + "Customer's Name:  "+ name + "\n" + "E-mail:  "+ email + "\n" + "Phone Number:   " + phone_number

            send_mail(
                subject,
                body_message,
                mail,
                to_email,
                fail_silently=False,
            )
            form = ContactoForm()
        else:
            extra = "Faltan Datos"
    else:
        form = ContactoForm()
    cars = Car.objects.all()
    car_list = []
    for car in cars:
        car_list.append([car, car.images.all(), car.images.all().count()])
    last_three = car_list[:]  # Copiando en una lista diferente
    last_three.reverse()
    context = {
        'form': form,
        'infoExtra': extra,
        'manufacturer_list': Manufacturer.objects.all(),
        'manager': Manager.objects.first(),
        'category_list': Category.objects.all(),
        'last_three': last_three[:3]
    }
    return render(request, "contact.html", context)

def meetManager(request):

    context = {
        'manager': Manager.objects.first(),
    }
    return render(request, "base.html", context)
