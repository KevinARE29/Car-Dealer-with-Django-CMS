from django import forms

class ContactoForm(forms.Form):
    name = forms.CharField( widget= forms.TextInput(attrs={'placeholder':'Enter your name here'}))
    email = forms.EmailField( widget= forms.EmailInput(attrs={'placeholder':'Enter your e-mail here'}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Enter your phone number here'}))
    car = forms.CharField( widget= forms.TextInput(attrs={'placeholder':'What type of car are you looking for?'}))
    message = forms.CharField( widget= forms.TextInput(attrs={'placeholder':'Enter the message here'}))
