from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

class CarDealerApp(CMSApp):
    name = "Car Dealer"
    urls = ["car_dealer_app.urls"]
    app_name = "car_dealer_app"

apphook_pool.register(CarDealerApp)