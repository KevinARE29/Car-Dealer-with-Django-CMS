from django.db import models
from django.core.validators import MinValueValidator
import datetime
# Create your models here.
FUEL_TYPE = (
    ('1', 'Petrol'), ('2', 'Diesel'), ('3', 'Electric')
)
TRANSMISSION = (
    ('1', 'Automatic'), ('2', 'Standar')
)
MIN_PRICE = 500
MAX_PRICE = 40000
MIN_YEAR = 2000
MAX_YEAR = datetime.datetime.now().year.__str__()
class Category(models.Model):
    name = models.CharField('Name', max_length=64, blank=False, null=False)
    image = models.ImageField(upload_to="category_images")
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'

class Feature(models.Model):
    name = models.CharField('Name', max_length=64, blank=False, null=False)

    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField('Name', max_length=64, blank=False, null=False)
    image = models.ImageField(upload_to="manufacturer_images")
    def __str__(self):
        return self.name

class Car(models.Model):
    name = models.CharField('Name', max_length=64, blank=False, null=False)
    description = models.TextField('Description')
    year = models.IntegerField('Year', blank=False, null=False, validators=[MinValueValidator(0)])
    price = models.DecimalField('Price', max_digits=8, decimal_places=2, blank=True, null=True, validators=[MinValueValidator(0)])
    doors = models.IntegerField('Doors', blank=False, null=False, validators=[MinValueValidator(0)], default=4)
    seats = models.IntegerField('Seats', blank=False, null=False, validators=[MinValueValidator(0)], default=5)
    mileage = models.DecimalField('Mileage', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)], default = 0)
    fuel_type = models.CharField('Fuel Type', max_length=1, choices=FUEL_TYPE, blank=False, null=False, default=('1'))
    transmission = models.CharField('Transmission', max_length=1, choices=TRANSMISSION, blank=False, null=False, default=('1'))
    category = models.ForeignKey(Category, null=False, blank=False)
    manufacturer = models.ForeignKey(Manufacturer, null=False, blank=False)
    features = models.ManyToManyField(Feature)

    def __str__(self):
        return self.name

    def get_fuel_type(self):
        for fuel_type in FUEL_TYPE:
            if self.fuel_type == fuel_type[0]:
                return fuel_type[1]

    def get_transmission(self):
        for transmission in TRANSMISSION:
            if self.transmission == transmission[0]:
                return transmission[1]

class CarImage(models.Model):
    car = models.ForeignKey(Car, related_name='images')
    image = models.ImageField(upload_to="car_images")

class Manager(models.Model):
    name = models.CharField('Managers Name', max_length=40)
    last_name = models.CharField('Managers Last Name', max_length=40)
    phone_number = models.CharField('Managers Phone Number', max_length=12, help_text='Formato: XXX-XXX-XXXX', unique=True)
    email = models.EmailField('Managers E-mail', max_length=254, blank=True, null=True, unique=True)
    job = models.CharField('Managers Job', max_length=40)
    facebook = models.CharField('Managers Facebook', max_length=70)
    twitter = models.CharField('Managers Twitter', max_length=70)
    instagram = models.CharField('Managers Instagram', max_length=70)
    photo = models.ImageField('Photograph', upload_to="manager_images")

    def __str__(self):
        return self.name